---
title: Workshops
---
### Organising a workshop
 * from Day3, you can hold workshop at card0 village between 12:00 and 23:00, in 1 hr slots, starting at the full hour, check which slots are already used in the list below
 * for other days/times please coordinate with lilafisch
 * the card10 area is suitable for small scale (10 to 20 people), hands-on workshops
 * feel free to hold card10 workshops at other locations (e.g. Johnson) as well, but it is up to you to organise the room
 * for help, ask lilafisch

### Participating
 * there is no sign-up, just turn up
 * check the worksop descriptions to see if you should bring anything/have anything pre-installed on your laptop
 * you might also find more card10 related sessions on the cccamp19 [self organised sessions page](https://events.ccc.de/camp/2019/wiki/Static:Self-organized_Sessions)
 
## Workshops
please keep chronological order

### Environmental and IMU sensor workshop
> ~~Time: Day 2, 19:00~~
> ~~Location: card10 village~~
  * Time: Day2, **20:00**
  * Location: Johnson
  * [Description](https://events.ccc.de/camp/2019/wiki/Session:Environmental_and_IMU_sensors_on_the_card10_badge)


### Rust l0dables on the card10 badge
  * Time: Day 2, 19:00
  * Location: Johnson
  * [Description](https://events.ccc.de/camp/2019/wiki/Session:Rust_l0dables_on_the_card10_badge)


### Micropython on card10 for complete beginners
  * Time: Day 3, 15:00
  * Location: card10 area
  * bring a laptop and a usb-c cable
  * and your card10
  * no knowledge of python needed

### E-textile card10 workshop
  * Time: Day 3, 19:00 - 20:30
  * Location: card10 area
  * bring: card10, laptop, USB-C cable
  * (EN) also useful: sewing needles, thread, through hole LEDs, needle nose pliers, snap fasteners
  * (DE) auch nuetzlich: naehnadeln, faden, 3mm oder 5mm LEDs (zum durchstecken), feine zangen zum draht biegen, druckknoepfe
 

### EKG fuer Hacker
  * Time: Day3, 21:00
  * Location: card10 area
  * EKG daten lesen praesentiert vom CERT