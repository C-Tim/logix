---
title: D1 - taveler Aki
---

## logbook entry

D1, traveler Aki
Since todays morning my view on card10 changed again when I found out about the possibilities of communication through color. Not only can one sync with other travelers by showing the same blinking theme while holding card10 high above ones head, there are also three colors that have a special meaning for everyone here. 
Three lights, framed by a yellow, a blue and a green circle are used to communicate ones scope of interaction with the world. If the yellow light is active travelers somehow seem to be in wild adventure mode or on some mission, they call it the chaos light. The blue one seems to stand for a general open and communicative mode, I saw travelers starting conversations a lot with blue-lighted others. The green one is a sign for a traveler to be in so called campmode which to me means to do selfcare, to not be visible for other travelers because I am fine with being for myself.
Since I started to use this interhacktion, experiencing the blue mode is most exciting to me because I am still not used to people just starting conversations with me and I am enjoying it a lot. I appreciate the comfort that comes with activating the green light, I can be totally with myself although I am floating around. 
It feels like a lot of different perspectives where included in designing all those interhacktions for card10, I wish I knew more about the travelers that developed all this...

## conclusions for this interhacktion:

card10 is supposed to have LEDs that communicate ones personal *scope of interaction*.

#### demands

* There are LEDs on the badge that are reserved for this feature (marked in a more persistent way).
* There are at least three led states (colors): I am in wild experiencing mode (chaos) / I am in a communicative mood (communication) / I don't want to interact (camp).
* The meaning of the LED states is well documented and announced, as they will only work if a critical mass of travelers knows about them. (If we decide to have only few states there could be a legend printed directly on the pcb)
* (There is the idea to find new words for personal states or to take elements from the camp design that will eventually be revealed, but at least the naming for the "I don't want to communicate"-state should be very clear and not be able to be interpreted in a wrong way)
* The LED state should be able to be set from the badge interface (without phone or other connected device)
* The LEDs should be readable from a proper distance where one doesn't need to enter the others personal space. We could either use a diffusor or multiple LEDs placed adjacent to each other for better readability
* This feature should be discussed with e. g. the autism community to include different perspectives and needs. 
